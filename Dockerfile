FROM docker.io/listmonk/listmonk:latest
ADD entrypoint.sh /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
